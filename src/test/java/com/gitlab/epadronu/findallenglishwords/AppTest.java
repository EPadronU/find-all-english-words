package com.gitlab.epadronu.findallenglishwords;

import java.net.ConnectException;
import java.util.Collections;
import java.util.Set;

import com.gitlab.epadronu.findallenglishwords.utils.AllureExtension;
import com.gitlab.epadronu.findallenglishwords.utils.PascalCaseDisplayNameGenerator;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static java.time.Duration.ofMinutes;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

@DisplayNameGeneration(PascalCaseDisplayNameGenerator.class)
@ExtendWith({AllureExtension.class})
@Tag("Functional")
@Severity(SeverityLevel.NORMAL)
public class AppTest {

  @Test
  @Tag("SadPath")
  void testInCaseOfConnectionFailure() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(() -> {throw new ConnectException();});

    assertThatIllegalStateException().isThrownBy(() -> finder.findAllWordsIn("WORKING"));
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfNullString() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(() -> Set.of("word"));

    assertThat(finder.findAllWordsIn(null)).isEmpty();
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfEmptyString() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(() -> Set.of("word"));

    assertThat(finder.findAllWordsIn("")).isEmpty();
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfBlankString() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(() -> Set.of("word"));

    assertThat(finder.findAllWordsIn("   \t    \n\n")).isEmpty();
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfAnEmptyCollectionOfKnownWords() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(Collections::emptySet);

    assertThat(finder.findAllWordsIn("WORKING")).isEmpty();
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfASingleWord() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(
     () -> Set.of("work", "king", "ring", "row"));

    assertThat(finder.findAllWordsIn("WORKING"))
     .containsExactlyInAnyOrder("WORK", "KING", "RING", "ROW");
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfASingleWordWithNonPrintableCharacters() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(
     () -> Set.of("work", "king", "ring", "row"));

    assertThat(finder.findAllWordsIn("\tWOR\n\nKI\t  N  G  "))
     .containsExactlyInAnyOrder("WORK", "KING", "RING", "ROW");
  }

  @Test
  @Tag("HappyPath")
  void testInCaseOfTwoOrMoreWords() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(
     () -> Set.of("sea", "coral", "car", "role", "cel", "ale", "leo", "rose", "other"));

    assertThat(finder.findAllWordsIn("sea coral"))
     .containsExactlyInAnyOrder("sea", "coral", "car", "role", "cel", "ale", "leo", "rose");
  }

  @Test
  @Tag("Non-functional")
  void testInCaseOfALongString() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(() -> Set.of("Word"));

    assertTimeoutPreemptively(ofMinutes(1), ()-> finder.findAllWordsIn("long string"));
  }

  @Test
  @Tag("Non-functional")
  void testInCaseOfATinyBitLongerString() {
    final EnglishWordsFinder finder = new EnglishWordsFinder(() -> Set.of("Word"));

    assertTimeoutPreemptively(ofMinutes(1), ()-> finder.findAllWordsIn("a long string"));
  }
}
